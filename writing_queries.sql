-- Task 1. Solution 1:
WITH RevenuePerStaff AS (
    SELECT
        s.staff_id,
        s.store_id,
        s.first_name || ' ' || s.last_name AS staff_name,
        SUM(p.amount) AS total_revenue,
        RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS rank
    FROM
        staff s
    INNER JOIN
        payment p ON s.staff_id = p.staff_id
    WHERE
        p.payment_date BETWEEN '2017-01-01' AND '2017-12-31'
    GROUP BY
        s.staff_id, s.store_id
)
SELECT
    store_id,
    staff_name,
    total_revenue
FROM
    RevenuePerStaff
WHERE
    rank = 1;


-- Task 1. Solution 2:
SELECT
    s.first_name || ' ' || s.last_name AS staff_name,
    MAX(total_revenue) AS total_revenue
FROM
    staff s
INNER JOIN (
    SELECT
        staff_id,
        SUM(amount) AS total_revenue
    FROM
        payment
    WHERE
        payment_date BETWEEN '2017-01-01' AND '2017-12-31'
    GROUP BY
        staff_id
) AS p ON s.staff_id = p.staff_id
GROUP BY
    s.staff_id
ORDER BY
    total_revenue DESC;

-- Task 2. Solution 1:
WITH TopMovies AS (
    SELECT
        f.title,
        COUNT(r.rental_id) AS rental_count,
        AVG(EXTRACT(YEAR FROM CURRENT_DATE) - f.release_year) AS expected_age
    FROM
        film f
    INNER JOIN
        inventory i ON f.film_id = i.film_id
    INNER JOIN
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.title, f.release_year
    ORDER BY
        rental_count DESC
    LIMIT 5
)
SELECT
    title,
    rental_count,
    ROUND(expected_age) AS expected_age
FROM
    TopMovies;

-- Task 2. Solution 2:
SELECT
    f.title,
    COUNT(r.rental_id) AS rental_count,
    AVG(EXTRACT(YEAR FROM CURRENT_DATE) - f.release_year) AS expected_age
FROM
    film f
INNER JOIN
    inventory i ON f.film_id = i.film_id
INNER JOIN
    rental r ON i.inventory_id = r.inventory_id
GROUP BY
    f.title, f.release_year
ORDER BY
    rental_count DESC
LIMIT 5;

-- Task 3. Solution 1:
WITH ActorActivity AS (
    SELECT
        actor_id,
        MAX(last_update) AS last_activity
    FROM
        film_actor
    GROUP BY
        actor_id
)
SELECT
    a.first_name || ' ' || a.last_name AS actor_name,
    (DATE_PART('year', CURRENT_DATE) - DATE_PART('year', aa.last_activity)) AS years_inactive
FROM
    actor a
LEFT JOIN
    ActorActivity aa ON a.actor_id = aa.actor_id
ORDER BY
    years_inactive DESC
LIMIT 1;


-- Task 3. Solution 2:
SELECT
    a.first_name || ' ' || a.last_name AS actor_name,
    DATE_PART('year', CURRENT_DATE) - DATE_PART('year', MAX(fa.last_update)) AS years_inactive
FROM
    actor a
LEFT JOIN
    film_actor fa ON a.actor_id = fa.actor_id
GROUP BY
    a.actor_id
ORDER BY
    years_inactive DESC
LIMIT 1;
